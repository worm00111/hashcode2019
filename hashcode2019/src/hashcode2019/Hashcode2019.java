package hashcode2019;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Hashcode2019 {
    public static void main(String[] args) {
        int numOfPhotos = 0;
        ArrayList<Photo> photos = null;
        Hashtable<String, Integer> uniqueTags = new Hashtable<>();
        
        try{
            String fileName = "a_example.txt";
            
            BufferedReader br = new BufferedReader(new FileReader(fileName) );
            try{
                StringBuilder sb = new StringBuilder();
                
                numOfPhotos = Integer.parseInt(br.readLine());
                
                photos = new ArrayList<>();
                
                for (int i = 0; i < numOfPhotos; i++) {
                    String line = br.readLine();
                    String photoInfo = line.substring(0, 4);
                    String tagsString = line.substring(4, line.length());
                    
                    String[] temp = photoInfo.split(" ");
                    boolean horizontal = "H".equals(temp[0]);
                    int numOfTags = Integer.parseInt(temp[1]);
                    
                    temp = tagsString.split(" ");
                    
                    photos.add(new Photo(i, temp, horizontal));
                    
//                    for (int j = 0; j < numOfTags; j++) {
//                        if (!uniqueTags.containsKey(photos[i].tags[j])) {
//                            uniqueTags.put(photos[i].tags[j], 1);
//                        } else {
//                            int count = uniqueTags.get(photos[i].tags[j]) + 1;
//                            uniqueTags.put(photos[i].tags[j], count);
//                        }
//                    }
                }
            }
            finally{
                br.close();
            }
         
//            Enumeration iterator = uniqueTags.keys();
//            while (iterator.hasMoreElements()) {
//                String key = (String) iterator.nextElement();
//                System.out.println("tag: " + key + " & count: " + uniqueTags.get(key));
//            }
        }
        catch(Exception e){
            System.out.println(e);
        }
        
        ArrayList<Slide> slideshow = CreateSlideshow(photos);
        
        int totalSlides = 0;
        
        String fileContent = "";
        fileContent+= slideshow.size();
        fileContent+="\n";
        
        for(int i=0;i<slideshow.size();i++){
            
            String toAdd = slideshow.get(i).toString();
            fileContent+=toAdd;
            fileContent+="\n";
            

        }
        
        FileWriter fw;
        try {
            fw = new FileWriter("C:\\Users\\Vilius\\Documents\\NetBeansProjects\\hashcode2019\\hashcode2019\\outputexample.txt");
            BufferedWriter writer = new BufferedWriter(fw);
            writer.write(fileContent);
            writer.close();
        }
        catch (IOException ex) {
            //Logger.getLogger(Hashcode2019.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public static ArrayList<Slide> CreateSlideshow(ArrayList<Photo> photos) {
        if (photos.size() == 0) return null;
        
        ArrayList<Slide> slideshow = new ArrayList<>();
        Photo p1 = photos.get(0);
        if (!p1.horizontal) {
            int counter = 1;
            Photo complimentingPhoto = null;
            do {
                if (!photos.get(counter).horizontal) {
                    complimentingPhoto = photos.get(counter);
                    photos.remove(p1);
                    photos.remove(complimentingPhoto);
                    p1 = Photo.combinePhotos(p1, complimentingPhoto);
                }
            } while (complimentingPhoto == null);
        }
        
        slideshow.add(new Slide(p1));
        photos.remove(p1);
        
        // Add if theres matching photos with tags left
        while (photos.size() >= 1) {
            if (!photos.get(0).horizontal && photos.size() == 1) break;
            
            Photo p2 = null;
            int currentScore = 0;
            int currentID = 0;
            boolean horizontalMatch = true;
            int complimentingVerticalPhotoID = 0;
            for (int i = 0; i < photos.size(); i++) {
                p2 = photos.get(i);
                
                if (!p2.horizontal) {
                    int ndID = checkVerts(p1.id, p2.id, photos.toArray());
                    // compare verticals
                    // return id of the highest score
                    // replace p2 with combined photo
                    // complimentingPhoto = GetPhotoWithID(photos, 2ndID);
                    // p2 = Photo.combinePhotos(p2, complimentingPhoto);
                }

                int p1UniqueTags = CalcUniqueTags(p1, p2);
                int sharedTags = CalcCommonTags(p1, p2);
                int p2UniqueTags = CalcUniqueTags(p2, p1);

                int lowestScore = Math.min(p2UniqueTags, Math.min(p1UniqueTags, sharedTags));
                
                if (lowestScore > currentScore ) {
                    currentID = photos.get(i).id;
                    currentScore = lowestScore;
                    if (!p2.horizontal) {
                        horizontalMatch = false;
                        //complimentingPhotoID = photo ID from erics method
                    }
                    else {
                        horizontalMatch = true;
                    }
                }
            }
            
            p1 = p2;
            
            if (horizontalMatch) {
                slideshow.add(new Slide(p1));
                
                RemovePhotoWithID(photos, currentID);
            }
            else {
                Photo firstPhotoToAdd = GetPhotoWithId(photos, currentID);
                Photo secondPhotoToAdd = GetPhotoWithId(photos, complimentingVerticalPhotoID);
                slideshow.add(new Slide(firstPhotoToAdd, secondPhotoToAdd));
                
                RemovePhotoWithID(photos, currentID);
                RemovePhotoWithID(photos, complimentingVerticalPhotoID);
                
            }
        }
        
        return slideshow;
    }
    
    public static Photo GetPhotoWithId(ArrayList<Photo> photos, int id) {
        for (int i = 0; i < photos.size(); i++) {
            if (photos.get(i).id == id) {
                return photos.get(i);
            }
        }
        return null;
    }
    
    public static void RemovePhotoWithID(ArrayList<Photo> photos, int id) {
        for (int i = 0; i < photos.size(); i++) {
            if (photos.get(i).id == id) {
                photos.remove(i);
            }
        }
    }
    
    public static int CalcCommonTags(Photo p1, Photo p2) {
        int count = 0;
        
        for (String p1Tag : p1.tags) {
            for (String p2Tag : p2.tags) {
                if (p1Tag.equals(p2Tag)) {
                    count++;
                }
            }
        }
        
        return count;
    }
    
    public static int CalcUniqueTags(Photo p1, Photo p2) {
        int count = p1.tags.length;
        
        for (String p1Tag : p1.tags) {
            for (String p2Tag : p2.tags) {
                if (p1Tag.equals(p2Tag)) {
                    count--;
                }
            }
        }
        
        return count;
    }
    
    public static int checkVerts(int photo, int vertical, ArrayList<Photo> photos)
	{
		ArrayList<String> photoTags = new ArrayList<String>(Arrays.asList(photos[photo].tags));
		ArrayList<String> verticalTags = new ArrayList<String>(Arrays.asList(photos[vertical].tags));
		ArrayList<String> tempTags; //current tags of index value
		ArrayList<String> compareTags = new ArrayList<String>(); //combined of vertical and temp
		
		int uID = -1;
		int uniqueTags1 = 0;
		int uniqueTags2 = 0; 
		int sharedTags = 0;
		int bestScore = 0;
		int previousBestScore = 0;
		
		for (Photo p: photos)
		{
			compareTags = verticalTags;
			if (!p.horizontal)
			{
				tempTags =  new ArrayList<String>(Arrays.asList(p.tags));
				for (int i = 0; i < tempTags.size(); i++)
					if (!compareTags.contains(tempTags.get(i)))
						compareTags.add(tempTags.get(i)); 
				sharedTags = CalcCommonTags(photos[photo], photos[p.id]);
				uniqueTags1 = CalcUniqueTags(photos[p.id], photos[photo]);
				uniqueTags2 = CalcUniqueTags(photos[photo], photos[p.id]); 
				bestScore = uniqueTags1 > uniqueTags2 ? uniqueTags2 : uniqueTags1;
				bestScore = bestScore > sharedTags ? sharedTags : bestScore;
				if(bestScore < previousBestScore)
				{
					previousBestScore = bestScore;
					uID = p.id;
				}
			}
		}
		
		return uID;
	}
    
    
}
