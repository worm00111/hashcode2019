/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hashcode2019;

/**
 *
 * @author Stephen
 */
public class Slide {
    private Photo p1;
    private Photo p2;
    
    public Slide(Photo p1, Photo p2){
        this.p1 = p1;
        this.p2 = p2;
    }
    public Slide(Photo p1){
        this.p1 = p1;
    }
    
    public String toString(){
        String s = "";
        s += p1.id;
        if(p2 != null){
            s+= " " + p2.id;
        }
        return s;
    }
}
