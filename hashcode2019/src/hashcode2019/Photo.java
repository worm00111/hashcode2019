package hashcode2019;

public class Photo {
    public boolean horizontal;
    public String[] tags;
    public int id;
    
    public Photo (int id, String[] tags, boolean horizontal) {
        this.tags = tags;
        this. horizontal = horizontal;
        this.id = id;
    }
    
    public static Photo combinePhotos(Photo p1, Photo p2){
        String [] arr = new String[p1.tags.length + p2.tags.length];
        int i = 0;
        for(; i < p1.tags.length; i++)
            arr[i] = p1.tags[i];
        for(; i < p1.tags.length + p2.tags.length; i++)
            arr[i] = p2.tags[i - p1.tags.length];
        
        return new Photo(0, arr, true);
    }
}
